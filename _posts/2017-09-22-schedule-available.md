---
title: Schedule now available!
date: 2017-09-22 10:00:00 -0400
excerpt_separator: <!--more-->
---

The PyGotham 2017 schedule is now available. It features three tracks spread
across October 6th and 7th.

<!--more-->

This year's program features 63 speakers giving 60 talks covering a wide array
of topics, from application design to data science to natural language
processing to web development. There will also be three keynote talks and
lightning talks.

We're really excited about this year's schedule and hope that you are too.
[Be sure to check it out!]({% link talks/schedule.md %})

Thanks to everyone who submitted a talk, voted on talks, or signed up for the
program committee.
