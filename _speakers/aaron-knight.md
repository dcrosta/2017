---
name: Aaron Knight
talks:
- 'Fixing web data in production: best practices for bad situations'
---

Aaron Knight is a full-stack engineer at Voxy, a company that creates dynamic, personalized educational courses for English learners.